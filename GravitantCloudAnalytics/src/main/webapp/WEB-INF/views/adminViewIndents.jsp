                                                                <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Services | Corlate</title>
    
    <!-- core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="resources/css/animate.min.css" rel="stylesheet">
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="resources/css/responsive.css" rel="stylesheet">
	<link href="resources/css/jquery.motionCaptcha.0.2.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="resources/js/html5shiv.js"></script>
    <script src="resources/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +0123 456 70 90</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                               <!--  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li> -->
								<li class="active"><a href="login.jsp" style="width: 90px;height: 38px;padding: 7px;background: #c52d2f;">Log Out</a></li>
                            </ul>
                            
                            <div class="search">
                                <form role="form" action="#" method="POST">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                           </div>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <a class="navbar-brand" href="index.jsp">Servoil</a>
				  
                </div>
                 <div style="float:right;color:#FFFFFF;"> Welcome! Admin </div>
                <div class="collapse navbar-collapse navbar-right">
                    <!--  <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about-us.html">About Us</a></li>
                        <li ><a href="services.html">Services</a></li>
                        <li><a href="portfolio.html">Portfolio</a></li>
                       
                        <li><a href="contact-us.html">Contact</a></li>                        
                    </ul> -->
                                              
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->

    <section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="container">
    <div class="row">
        
         
                  <div class="col-md-3 col-sm-6">
				  <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
                   <div class="list-group">
        <span href="#" class="list-group-item active">
            Account Details
            <span class="pull-right" id="slide-submenu">
                <i class="fa fa-times"></i>
            </span>
        </span>
		<a href="admin_view.jsp" class="list-group-item"  style="background-color: #f5f5f5;" >
       <div align="left">Placed Intents </div>
        </a>
        <a href="recentlyplacedindents.jsp" class="list-group-item" >
       <div align="left" >Recently Processed Indents</div>
        </a>
         <a href="accountdetails.jsp" class="list-group-item" >
       <div align="left" >Place A order</div>
        </a>
        <a href="addnewproduct.jsp" class="list-group-item" >
      <div align="left"> Add A new Product/Indent </div>
        </a>
        <a href="Cancelled_indents.jsp" class="list-group-item" >
      <div align="left"> Cancelled Indents </div>
        </a>
       
    </div>  
                </div><!--/.col-md-3-->
				<div class="col-md-8 col-sm-6">
				
				<div class="table-responsive">
					   <table class="table">
					  <thead>
							  <tr>
								<th align="centre">Customer Id</th>
								<th align="centre">Sku Code</th>
								<th align="centre">Item Name</th>
								<th align="centre">SalesPrice PerPack</th>
								
								<th align="centre"></th>
							  </tr>
						</thead>
							  <tbody id="id01">
	    
		  	<tr>
		  	<td align="left">12559</td>
            <td align="left">2900125</td>
            <td align="left">2T 300*40 Ml 03/09</td>
            <td align="left">2572.89</td>
           
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">15459</td>
            <td align="left">2900103</td>
            <td align="left">2T 600*20 Ml 03/09</td>
            <td align="left">3055.01</td>
          
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">25459</td>
            <td align="left">2900401</td>
            <td align="left">2T SUPREME 210 Ltr</td>
            <td align="left">42183.4</td>
          
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">125859</td>
            <td align="left">2949180</td>
            <td align="left">4 T SYNTHETIC 24*1 Ltr</td>
            <td align="left">10960.12</td>
           
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">1259</td>
            <td align="left">2924129</td>
            <td align="left">4T 10 X (800+100 Ml) 900 Ml 01/07</td>
            <td align="left">1695.8</td>
           
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">1254</td>
            <td align="left">2924130</td>
            <td align="left">4T 20 X (800+100 Ml) 900 Ml 03/09</td>
            <td align="left">3478.94</td>
         
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></td>
          </tr>
		  	<tr>
		  	<td align="left">1459</td>
            <td align="left">2924207</td>
            <td align="left">4T 20 X (900+100 Ml) 1 Ltr 03/09</td>
            <td align="left">3828.3</td>
           
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></a></td>
          </tr>
		  	<tr>
		  	<td align="left">159</td>
            <td align="left">2943650</td>
            <td align="left">4T GOBEN 10*900 Ml</td>
            <td align="left">1936</td>
       
           <td align="left"><input type='submit' name='submit' value='View Detail' class='btn btn-lg btn-primary btn-block viewIndent' id='detect_rel2'></a></td>
          </tr>
		<div id="indentDetails" style="position: absolute;
top: 81px;
left: 20%;
background: #f7f7f7;
border: 10px solid #4e4e4e;display:none;width:65%;">
				<div class="col-md-12 col-sm-6" style="">
					<h2> Detail Indent View </h2>
					<hr>
				<div class="col-md-12 col-sm-6 register"  id="register_div1" style="padding:30px;">
					<div class="col-md-12 col-sm-6 register alert-danger "  id="register_div1" >
						<div class="input" class="col-md-6 col-sm-6">	
							<label style="float:left; width:200px;">SKU Code </label>
						</div>
						<div class="input" class="col-md-6 col-sm-6">
							 <span style="font-size: 1.2em; float:left;" >2900175</span><br>
						</div>
					</div>	
					<div class="col-md-12 col-sm-6 register"  id="register_div1" >
						<div class="input" class="col-md-6 col-sm-6" >	
							<label style="float:left; width:200px;">Item Name </label>
						</div>
						<div class="input" class="col-md-6 col-sm-6">
							 <span style="font-size: 1.2em; float:left;" >2T 20*1/2 Ltr 03/09</span><br>
						</div>
					</div>	
					<div class="col-md-12 col-sm-6 register alert-danger"  id="register_div1" >
						<div class="input" class="col-md-6 col-sm-6" >	
							<label style="float:left; width:200px;">SalesPrice PerPack </label>
						</div>
						<div class="input" class="col-md-6 col-sm-6">
							 <span style="font-size: 1.2em; float:left;" >2071.27</span><br>
						</div>
					</div>	
					<div class="col-md-12 col-sm-6 register"  id="register_div1" >
						<div class="input" class="col-md-6 col-sm-6" >	
							<label style="float:left; width:200px;">MRP </label>
						</div>
						<div class="input" class="col-md-6 col-sm-6">
							 <span style="font-size: 1.2em; float:left;" >134</span><br>
						</div>
					</div>	
					<div class="col-md-12 col-sm-6 register alert-danger"  id="register_div1" >
						<div class="input" class="col-md-6 col-sm-6" >	
							<label style="float:left; width:200px;">Quantity </label>
						</div>
						<div class="input" class="col-md-6 col-sm-6">
							 <span style="font-size: 1.2em; float:left;" >6</span><br>
						</div>
					</div>	
					
					
								</div>
					<div class="input">	
							
								 <input type="button" value="Cancel Indent" id="closeIndent">
								 <input type="button" value="Process Indent" >
								 <input type="button" value="Convert To Pdf" >
								 <input type="button" value="Convert To Excel"> 				 
					</div>

								</div>
											
								<br>

								
									</div>
		  </div>
							</tbody>
							
							

<!-- <script>
var xmlhttp = new XMLHttpRequest();
var url = "skudetails.txt";

xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        myFunction(xmlhttp.responseText);
    }
}
xmlhttp.open("GET", url, true);
xmlhttp.send();

function myFunction(response) {
    var arr = JSON.parse(response);
    var i;
    var out = "<tbody>";

    for(i = 0; i < arr.length; i++) {
        out += "<tr class='mytableinfo'><td class='selector'>" +
        arr[i].Sku_Code +
        "</td><td class='selector'>" +
        arr[i].Item_Name +
        "</td><td class='selector'>" +
        arr[i].SalesPrice_PerPack +
        "</td><td class='selector'>" +
        arr[i].MRP +
        "</td><td class='selector'>" +
        arr[i].Quantity +
        "</td><td><input type='submit' name='submit' value='Booked Indents' class='btn btn-lg btn-primary btn-block' id='detect_rel2'></td></tr>";
    }
    out += "</tbody>"
    document.getElementById("id01").innerHTML = out;
}
</script> -->



					  </table>
					</div>
</div>
               
         
       
        
    </div>
</div>
            </div>

            


           

        </div><!--/.container-->
    </section><!--/#feature-->


    

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="resources/js/jquery.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/jquery.prettyPhoto.js"></script>
    <script src="resources/js/jquery.isotope.min.js"></script>
    <script src="resources/js/main.js"></script>
    <script src="resources/js/wow.min.js"></script>
	<script src="resources/js/jquery.motionCaptcha.0.2.js"></script>
</body>
</html>
<script>
$(function(){

	$('#slide-submenu').on('click',function() {			        
        $(this).closest('.list-group').fadeOut('slide',function(){
        	$('.mini-submenu').fadeIn();	
        });
        
      });

	$('.mini-submenu').on('click',function(){		
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
	})
})

</script>

<script type="text/javascript">
$(document).ready(function(){
	$(".viewIndent").click(function(){
		$("#indentDetails").show();
	});
	$("#closeIndent").click(function(){
		$("#indentDetails").hide();
	});
	});
</script>
<style>
.mini-submenu{
  display:none;  
  background-color: rgba(0, 0, 0, 0);  
  border: 1px solid rgba(0, 0, 0, 0.9);
  border-radius: 4px;
  padding: 9px;  
  /*position: relative;*/
  width: 42px;

}

.mini-submenu:hover{
  cursor: pointer;
}

.mini-submenu .icon-bar {
  border-radius: 1px;
  display: block;
  height: 2px;
  width: 22px;
  margin-top: 3px;
}

.mini-submenu .icon-bar {
  background-color: #000;
}

#slide-submenu{
  background: rgba(0, 0, 0, 0.45);
  display: inline-block;
  padding: 0 8px;
  border-radius: 4px;
  cursor: pointer;
}
</style>


	
	
                            
                            