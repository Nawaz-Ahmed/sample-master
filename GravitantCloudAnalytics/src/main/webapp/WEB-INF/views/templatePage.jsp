<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Servoil</title>

<!-- core CSS -->
		<link href="resources/css/bootstrap.min.css" rel="stylesheet">
		<link href="resources/css/font-awesome.min.css" rel="stylesheet">
		<link href="resources/css/animate.min.css" rel="stylesheet">
		<link href="resources/css/main.css" rel="stylesheet">
		<link href="resources/css/responsive.css" rel="stylesheet">
		<link href="resources/css/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<!--/head-->

<body>
	<jsp:include page="header.jsp"></jsp:include>

	<section id="feature" class="transparent-bg">
		<div class="container">
			<div class="center wow fadeInDown" data-wow-duration="1000ms"
				data-wow-delay="600ms">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-3 wow fadeInUp">
							<jsp:include page="userSideMenu.jsp"></jsp:include>
						</div>
						<div class="col-xs-12 col-sm-9 wow fadeInDown" align="center">
							<!-- Body Content Comes here -->
						</div>
						<div class=" col-xs-12 col-sm-9 col-md-9">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#feature-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/#footer-->

	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<%--  <script src="resources/js/jquery.prettyPhoto.js"></script> --%>
	<%--     <script src="resources/js/jquery.isotope.min.js"></script> --%>
	<script src="resources/js/main.js"></script>
	<%--   <script src="resources/js/wow.min.js"></script> --%>
	<%-- 	<script src="resources/js/jquery.motionCaptcha.0.2.js"></script> --%>
	<%-- <script src="resources/js/jquery.min.js"></script> --%>
	<script src="resources/js/jquery.dataTables.js"></script>
	<script src="resources/js/indent-item.js"></script>
</body>
</html>


