<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Services | Corlate</title>
    
    <!-- core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="resources/css/prettyPhoto.css" rel="stylesheet">
    <link href="resources/css/animate.min.css" rel="stylesheet">
    <link href="resources/css/main.css" rel="stylesheet">
    <link href="resources/css/responsive.css" rel="stylesheet">
	<link href="resources/css/jquery.motionCaptcha.0.2.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="resources/js/html5shiv.js"></script>
    <script src="resources/js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>

    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +0123 456 70 90</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                               <!--  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li> 
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype"></i></a></li> -->
								<li class="active"><a href="login.jsp" style="width: 90px;
height: 38px;
padding: 7px;background: #c52d2f;">Log Out</a></li>
                            </ul>
                            </ul>
							
                            <div class="search">
                                <form role="form" action="#" method="POST">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                           </div>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <a class="navbar-brand" href="index.jsp">Servoil</a>
                </div>
                
                <div class="collapse navbar-collapse navbar-right">
                     <!-- <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="about-us.html">About Us</a></li>
                        <li ><a href="services.html">Services</a></li>
                        <li><a href="portfolio.html">Portfolio</a></li>
                       
                        <li><a href="contact-us.html">Contact</a></li>                        
                    </ul> -->
                                              
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        
    </header><!--/header-->

    <section id="feature" class="transparent-bg">
        <div class="container">
           <div class="center wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="container">
    <div class="row">
        <form role="form">
         
                  <div class="col-md-3 col-sm-6">
				  <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
                   <div class="list-group">
        <span href="#" class="list-group-item active">
            Account Details
            <span class="pull-right" id="slide-submenu">
                <i class="fa fa-times"></i>
            </span>
        </span>
		<a href="admin_view.jsp" class="list-group-item"   >
       <div align="left">Placed Intents </div>
        </a>
        <a href="recentlyplacedindents.jsp" class="list-group-item" >
       <div align="left" >Recently Processed Indents</div>
        </a>
         <a href="accountdetails.jsp" class="list-group-item" >
       <div align="left" >Place A order</div>
        </a>
        <a href="addnewproduct.jsp" class="list-group-item" style="background-color: #f5f5f5;">
      <div align="left"> Add A new Product/Indent </div>
        </a>
        <a href="Cancelled_indents.jsp" class="list-group-item"  >
      <div align="left"> Cancelled Indents </div>
        </a>
       
    </div> 
                </div><!--/.col-md-3-->
				<div class="col-md-8 col-sm-6">
				
		
 
        <form role="form">
           
                
                <div class="form-group">
                   
                    <div class="input-group">
                        <input type="text" class="form-control" name="SKUName" id="InputName" placeholder="SKU" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                  
                    <div class="input-group">
                        <input type="email" class="form-control" id="InputEmailFirst" name="itemname" placeholder="Item Name" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                   
                    <div class="input-group">
                        <input type="email" class="form-control" id="InputEmailSecond" name="Salesperpack" placeholder="Sales Per Pack" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				<div class="form-group">
                   
                    <div class="input-group">
                        <input type="text" class="form-control" id="InputEmailSecond" name="mrp" placeholder="MRP" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
			
				
               
              
				<input type="submit" name="submit" id="submit"  value="Submit Form" class="btn btn-info pull-right" />
                <input type="hidden" id="fairly-unique-id" value="submitform.php" />
         
        </form>
        
   

</div>
               
         
        </form>
        
    </div>
</div>
            </div>

            


           

        </div><!--/.container-->
    </section><!--/#feature-->


    

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="resources/js/jquery.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/jquery.prettyPhoto.js"></script>
    <script src="resources/js/jquery.isotope.min.js"></script>
    <script src="resources/js/main.js"></script>
    <script src="resources/js/wow.min.js"></script>
	<script src="resources/js/jquery.motionCaptcha.0.2.js"></script>
</body>
</html>
<script>
$(function(){

	$('#slide-submenu').on('click',function() {			        
        $(this).closest('.list-group').fadeOut('slide',function(){
        	$('.mini-submenu').fadeIn();	
        });
        
      });

	$('.mini-submenu').on('click',function(){		
        $(this).next('.list-group').toggle('slide');
        $('.mini-submenu').hide();
	})
})

</script>


<style>
.mini-submenu{
  display:none;  
  background-color: rgba(0, 0, 0, 0);  
  border: 1px solid rgba(0, 0, 0, 0.9);
  border-radius: 4px;
  padding: 9px;  
  /*position: relative;*/
  width: 42px;

}

.mini-submenu:hover{
  cursor: pointer;
}

.mini-submenu .icon-bar {
  border-radius: 1px;
  display: block;
  height: 2px;
  width: 22px;
  margin-top: 3px;
}

.mini-submenu .icon-bar {
  background-color: #000;
}

#slide-submenu{
  background: rgba(0, 0, 0, 0.45);
  display: inline-block;
  padding: 0 8px;
  border-radius: 4px;
  cursor: pointer;
}
</style>

<script>
$(document).ready(function(){
var counter = 2;

$('.selector').click(function(){return false;});
$(".mytableinfo").click(function() {

    var tableData = $(this).children("td").map(function() {
        return $(this).text();
    }).get();

    alert("Your data is: " + $.trim(tableData[0]) + " , " + $.trim(tableData[1]) + " , " + $.trim(tableData[2])+ " , " + $.trim(tableData[3])+ " , " + $.trim(tableData[4]));
	
	
var element_div1 = "<section id=feature"+ counter + " class='transparent-bg'><div class='container' id=#new><div class='center wow fadeInDown' data-wow-duration='1000ms' data-wow-delay='600ms'><div class='table-responsive'><table class='table'><thead><tr><th align='centre'>&nbsp;&nbsp;Sku Code</th><th align='centre'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Name</th><th align='centre'>&nbsp;&nbsp;SalesPrice PerPack</th><th align='centre'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MRP</th><th align='centre'>Quantity</th><th align='centre'></th></tr></thead><tbody><tr class='mytableinfo'><td class='selector'>" + $.trim(tableData[0]) + "</td><td class='selector'>" + $.trim(tableData[1]) + "</td><td class='selector'>" + $.trim(tableData[2]) + "</td><td class='selector'>" + $.trim(tableData[3]) + "</td><td class='selector'>" + $.trim(tableData[4]) + "</td><td><input type='submit' name='submit' value='Cancel' class='btn btn-lg btn-primary btn-block detect_rel' ></td></tr></tbody></table></div></div></div></section>";  
	
	
	 $("section#feature").append(element_div1); 
	 counter++;
	 
});



});

	</script>
	
	