<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Services | Corlate</title>

<!-- core CSS -->
<link href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css" rel="stylesheet">
		<link href="resources/css/bootstrap.min.css" rel="stylesheet">
		<link href="resources/css/font-awesome.min.css" rel="stylesheet">
		<!-- <link href="resources/css/prettyPhoto.css" rel="stylesheet"> -->
		<link href="resources/css/animate.min.css" rel="stylesheet">
		<link href="resources/css/main.css" rel="stylesheet">
		<link href="resources/css/responsive.css" rel="stylesheet">
		<!-- <link href="resources/css/jquery.motionCaptcha.0.2.css" rel="stylesheet"> -->
		<link href="resources/css/jquery.dataTables.min.css" rel="stylesheet"
	type="text/css">

<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
		<link rel="shortcut icon" href="images/ico/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="144x144"
			href="images/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114"
			href="images/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72"
			href="images/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed"
			href="images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css">
	input#itemqty {
		width: 65px;
	}
	</style>
	<style>
	.indent-details {
		list-style-type: none;
		text-align: left;
	}
	
	.mini-submenu {
		display: none;
		background-color: rgba(0, 0, 0, 0);
		border: 1px solid rgba(0, 0, 0, 0.9);
		border-radius: 4px;
		padding: 9px;
		/*position: relative;*/
		width: 42px;
	}
	
	.mini-submenu:hover {
		cursor: pointer;
	}
	
	.mini-submenu .icon-bar {
		border-radius: 1px;
		display: block;
		height: 2px;
		width: 22px;
		margin-top: 3px;
	}
	
	.mini-submenu .icon-bar {
		background-color: #000;
	}
	
	#slide-submenu {
		background: rgba(0, 0, 0, 0.45);
		display: inline-block;
		padding: 0 8px;
		border-radius: 4px;
		cursor: pointer;
	}
	
	.form-horizontal .control-label {
		text-align: left;
	}
	
	p {
		margin: 0px 0px 0px;
		text-align: left;
	}
	
	.form-group {
		margin-bottom: 10px;
	}
	
	.btn-block+.btn-block {
		margin-top: 10px !important;
	}
	</style>
</head>
<!--/head-->

<body>
	<jsp:include page="header.jsp"></jsp:include>

	<section id="feature" class="transparent-bg">
		<div class="container">
			<div class="center wow fadeInDown" data-wow-duration="1000ms"
				data-wow-delay="600ms">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-3 wow fadeInUp">
							<jsp:include page="userSideMenu.jsp"></jsp:include>
						</div>
						


						<div class="col-xs-12 col-sm-9 wow fadeInDown">
							<div class="">
								<div class="" style="display: none;" id="indent-div">
									<div class="">
										<input type="hidden" name="user_id" value="${user.id}"
											id="Servouser_id">
										<form class="form-horizontal">
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Sku
													Code :</label>
												<div class="col-sm-4" style="padding-top: 7px;">
													<p id="skuCode"></p>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Item
													Name :</label>
												<div class="col-sm-4" style="padding-top: 7px;">
													<p id="itemName"></p>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">SalesPrice
													PerPack :</label>
												<div class="col-sm-4" style="padding-top: 7px;">
													<p id="pricePerPack"></p>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Mrp
													:</label>
												<div class="col-sm-4" style="padding-top: 7px;">
													<p id="mrp"></p>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Quantity
													:</label>
												<div class="col-sm-4" style="padding-top: 7px;" align="left">
													<input type="text" name="qty" id="qty">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Total
													:</label>
												<div class="col-sm-4" style="padding-top: 7px;" align="left">
													<input type="text" name="total" id="total">
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Remarks
													:</label>
												<div class="col-sm-4" style="padding-top: 7px;" align="left">
													<textarea cols="20" rows="" required="required"
														name="remarks" id="remarks"></textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="inputPassword3" class="col-sm-3 control-label">Action
													:</label>
												<div class="col-sm-4" style="padding-top: 7px;" align="left">
													<input type="submit" value="Add Item"
														onclick="addIndentItem()"
														class='btn btn-lg btn-primary btn-block mytableinfo'
														style="margin-top: 0px; padding: 5px 21px;">
												</div>
											</div>
										</form>


									</div>

								</div>
							</div>
							<div  class="col-xs-12 col-sm-12 wow fadeInDown" id="">
			 <div class="table-responsive" >
						   <table class="table table-hover">
							  <thead>
									  <tr>
									  	<th align="centre">Customer Id</th>
										<th align="centre">Sku Code</th>
										<th align="centre">Item Name</th>
										<th align="centre">SalesPrice PerPack</th>
										<th align="centre">MRP</th>
										<th align="centre">Quantity</th>
										<th align="centre">Total</th>
										<th align="centre">Remark</th>
										
									</tr>
								</thead>
								 <tbody id="id02">
									<c:if test="${confirmedOrderList != null }">
										<c:forEach items="${confirmedOrderList}" var="confirmedOrderList">
											
											 <tr>
											 	<td align="left">${confirmedOrderList.userid}</td>
											 	<td align="left">${confirmedOrderList.skuCode}</td>
											 	<td align="left">${confirmedOrderList.itemName}</td>
											 	<td align="left">${confirmedOrderList.pricePerPack}</td>
											 	<td align="left">${confirmedOrderList.mrp}</td>
											 	<td align="left">${confirmedOrderList.qty}</td>
											 	<td align="left">${confirmedOrderList.total}</td>
											 	<td align="left">${confirmedOrderList.remarks}</td>
											 	
											 	
											 	
											 </tr>
										</c:forEach>
									</c:if> 
								 </tbody>
						  </table>
					</div>				
				 </div>


						</div>

						<div class=" col-xs-12 col-sm-9 col-md-9"></div>
					</div>

				</div>
			</div>
		</div>






	</section>
	<!--/#feature-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/#footer-->

	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<%--  <script src="resources/js/jquery.prettyPhoto.js"></script> --%>
	<%--     <script src="resources/js/jquery.isotope.min.js"></script> --%>
	<!--<script src="resources/js/main.js"></script> -->
	<%--   <script src="resources/js/wow.min.js"></script> --%>
	<%-- 	<script src="resources/js/jquery.motionCaptcha.0.2.js"></script> --%>
	<%-- <script src="resources/js/jquery.min.js"></script> --%>
	<script src="resources/js/jquery.dataTables.js"></script>
	<script src="resources/js/indent-item.js"></script>
	

</body>
</html>


