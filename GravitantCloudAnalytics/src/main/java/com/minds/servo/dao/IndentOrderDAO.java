package com.minds.servo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Component;

import com.minds.servo.helper.ServoConstants;
import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.User;


@Component("indentOrderDAO")
public interface IndentOrderDAO {
	
	public String INITIATE_INSERT_INTO_INDENT_ORDER = "INSERT INTO indentorders (id,skucode,itemname,priceperpack,mrp,quantity,remarks,userid,orderstatus,total)"
			+ " VALUES (#{id},#{skuCode},#{itemName},#{pricePerPack},#{mrp},#{qty},#{remarks},#{userid},#{orderstatus},#{total})";
	
	
	
	@Insert(INITIATE_INSERT_INTO_INDENT_ORDER)
	@SelectKey(statement="select uuid() as id", keyProperty="id", before=true, resultType=String.class)
	@Options(useGeneratedKeys = true, keyProperty = "id", flushCache=true)
	public void insertInitiateIndentOrders(IndentDetails indentDetails);
	
	
	@Select("SELECT * FROM indentorders WHERE userid = #{userid} AND orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	
	
	public List<IndentDetails> getIndentOrdersByUserAndStatus(@Param("userid") String userid, @Param("orderstatus") String orderstatus);

	
	//cancelIndentOrderByUserAndOrderId
	@Select("update indentorders set orderstatus = '2' WHERE userid = #{userid} AND id= #{indentOrderId}")
	
	public void cancelIndentOrderByUserAndOrderId(@Param("userid") String userid, @Param("indentOrderId") String indentOrderId);

	@Select("update indentorders set orderstatus = '6' WHERE userid = #{userid} AND id= #{indentOrderId}")
	public void confirmIndentOrderByUserAndOrderId(@Param("userid") String userId,@Param("indentOrderId") String indentOrderId);

	
	@Select("SELECT * FROM indentorders WHERE  orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	
	public List<IndentDetails> getOpenIndentDetailsForAdmin(
			String confirmedIndentOrdersStatus);
	
	
	@Select("SELECT * FROM indentorders WHERE  userid= #{customerId}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	


	public List<IndentDetails> getcustomerIndentDetails(@Param("customerId") String customerId);
	
	
	@Select("SELECT * FROM indentorders WHERE userid=#{userid} AND orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	

public List<IndentDetails> getIndentOrdersByUserAndStatus1(@Param("userid") String userid, @Param("orderstatus") String orderstatus);

	@Select("update indentorders set orderstatus = '7' WHERE id= #{indentOrderId}")
	public void adminconfirmIndentOrderByUserAndOrderId(@Param("indentOrderId") String indentOrderId);

	
	
	@Select("update indentorders set orderstatus = '4' WHERE id= #{indentOrderId}")
	public void admincancelIndentOrderByUserAndOrderId(@Param("indentOrderId") String indentOrderId);

	
	
	@Select("SELECT * FROM indentorders WHERE  orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	
	public List<IndentDetails> getadminprocessedIndentDetailsForAdmin(String confirmedIndentOrdersStatus);

	@Select("SELECT * FROM indentorders WHERE  orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
	})
	

	public List<IndentDetails> getadmincancelledIndentOrdersByUserAndStatus1(String adminCancelledIndentOrdersStatus);

	@Select("SELECT * FROM indentorders WHERE  orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
		
	})	
	public List<IndentDetails> getrecentlyplacedIndentDetailsForAdmin(String adminConfirmedIndentOrdersStatus);
	
	@Select("SELECT * FROM indentorders WHERE  orderstatus= #{orderstatus}")
	@Results({
		@Result(property = "id", column = "id"),
		@Result(property = "skuCode", column = "skucode"),
		@Result(property = "itemName", column = "itemname"),
		@Result(property = "pricePerPack", column = "priceperpack"),
		@Result(property = "mrp", column = "mrp"),
		@Result(property = "qty", column = "quantity"),
		@Result(property = "remarks", column = "remarks"),		
		@Result(property = "userid", column = "userid"),
		@Result(property = "Servouser_id", column = "Servouser_id"),
		@Result(property = "orderstatus", column = "orderstatus"),
		@Result(property = "rowstate", column = "rowstate"),
		@Result(property = "total", column = "total")
	})

	public List<IndentDetails> getdeletedindentents(String adminCancelledIndentOrdersStatus);


	
	
	
	 
	
	
}


