package com.minds.servo.helper;

public class ServoConstants {
	
	public static final String CONFIRMED_INDENT_ORDERS_STATUS = "6";
	public static final String CANCELLED_INDENT_ORDERS_STATUS = "2";
	public static final String NEW_INDENT_ORDERS_STATUS = "1";
	public static final String DELETED_INDENT_ORDERS_STATUS = "3";
	public static final String ADMIN_CONFIRMED_INDENT_ORDERS_STATUS = "7";
	public static final String ADMIN_CANCELLED_INDENT_ORDERS_STATUS = "4";
	
	

}
