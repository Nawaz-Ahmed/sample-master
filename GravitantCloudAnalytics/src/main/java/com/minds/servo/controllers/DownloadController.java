package com.minds.servo.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
public class DownloadController 
{
	
	@RequestMapping(value = "/demo", method = RequestMethod.GET)
    public String showform(Locale locale, Model model) {
        return "demo";
    }
	
	@RequestMapping(value ="/export-indent-details", method = RequestMethod.GET)
    public void downloadnotepadfile(HttpServletRequest request,
            HttpServletResponse response) throws IOException 
    {
	response.setContentType("text/xlsx");  
	PrintWriter out = response.getWriter();  
	String filename = "formulaDemo.xlsx";   
	String filepath = "d:\\";   
	response.setContentType("APPLICATION/xlsx");   
	response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");   
	  
	FileInputStream fileInputStream = new FileInputStream(filepath + filename);  
	            
	int i;   
	while ((i=fileInputStream.read()) != -1) {  
	out.write(i);   
	}   
	fileInputStream.close();   
	out.close();   
	}  
	  
	}  
