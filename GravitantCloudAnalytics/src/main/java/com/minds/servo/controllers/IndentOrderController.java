package com.minds.servo.controllers;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;





import com.minds.servo.iservice.IndentOrderService;
import com.minds.servo.iservice.MailService;

import com.minds.servo.iservice.UserService;
import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.Product;
import com.minds.servo.model.User;
import com.minds.servo.service.impl.MailServiceImpl;

@Controller
public class IndentOrderController {
	
	@Autowired private IndentOrderService indentOrderService;
	@Autowired  private MailService mailservice;
	@Autowired private IndentDetails indentDetails;
	@Autowired private User user;
	@Autowired private UserService userservice;
	

	@RequestMapping(value = "/initiateIndentOrder",method=RequestMethod.POST)
	public String initiateIndentOrder(HttpServletResponse response,Model model,HttpSession session,String id,String skucode,String itemname,String priceperpack, String mrp, String qty, String remarks,String userid, String Servouser_id, String total)
	{		
       IndentDetails indentDetails = new IndentDetails();
		indentDetails.setId("123");
		indentDetails.setSkuCode(skucode);
		indentDetails.setItemName(itemname);
		indentDetails.setPricePerPack(priceperpack); 
		indentDetails.setMrp(mrp);
		indentDetails.setQty(qty);
		indentDetails.setRemarks(remarks);
		indentDetails.setOrderstatus("1");
		indentDetails.setServouser_id(Servouser_id);
		indentDetails.setTotal(total);
		//indentDetails.setCustOrgid("123");
		//indentDetails.setUserTypeid("123");
	//	indentDetails.setCreatedDate(createdDate);
	//	indentDetails.setLastUpdatedDate(lastupdatedDate);
		indentDetails.setUserid(Servouser_id);
	//	indentDetails.setRowstate("13");
		
		indentOrderService.initiateIndentOrder(indentDetails);
        System.out.println("order stored in db");  
		List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(userid);
		model.addAttribute("indentOrders", indentDetailsDB);
		return "redirect:/account-details";
		}
	
	//cancel-Indent
	@RequestMapping(value = "/cancel-Indent")
	public String cancelIndentOrder(HttpServletResponse response,Model model,HttpSession session,String indentOrderid){		
		
		
		User user = (User)session.getAttribute("user");
		
		indentOrderService.cancelIndentOrder(user.getId(),indentOrderid);
				
		//List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId(),"5");
		
		System.out.println("cancel-Indent in db");
		//model.addAttribute("indentOrders", indentDetailsDB);
		
		List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId());
		
		System.out.println("From Account Details Controller");
		model.addAttribute("indentOrders", indentDetailsDB);
		System.out.println("In account details controller");
		//return new ModelAndView("accountdetails");
		
		return "redirect:/account-details";
		
	}
	
	//confirm-Indent
	@RequestMapping(value = "/confirm-Indent")
	public String confirmIndentOrder(HttpServletResponse response,Model model,HttpSession session,String indentOrderId){		
		
		
		User user = (User)session.getAttribute("user");
		
	
		indentOrderService.confirmIndentOrder(user.getId(),indentOrderId);
		
		// if order confirmed , then mail send logic goes here
		
		mailservice.sendPlainMail(indentDetails,user.getEmailid());
		
				
		//List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId(),"5");
		
		System.out.println("confirm-Indent in db");
		//model.addAttribute("indentOrders", indentDetailsDB);
		
		
		return "redirect:/account-details";
		
	}
	
	
	@RequestMapping(value = "/user-confirmed-Indent-Orders")
	public ModelAndView confirmedIndentOrders(HttpServletResponse response,Model model,HttpSession session){		
		User user = (User)session.getAttribute("user");		
		List<IndentDetails> confirmedOrderList =  indentOrderService.confirmedIndentOrders(user);		
		System.out.println("confirm-Indent in db");
		model.addAttribute("confirmedOrderList", confirmedOrderList);	
		
		return new ModelAndView("userConfirmedIndents");
	}
	
	/*
		user-cancelled-indents
	 *	Controller to view indents cancelled by user
	 */
	@RequestMapping(value = "/user-cancelled-indents")
	public ModelAndView userCancelledIndentOrders(HttpServletResponse response,Model model,HttpSession session){		
		User user = (User)session.getAttribute("user");		
		List<IndentDetails> cancelledOrderList =  indentOrderService.cancelledIndentOrders(user);		
		model.addAttribute("cancelledOrderList", cancelledOrderList);	
		return new ModelAndView("userCancelledIndents"); 
	}

	
	/*
	 * /admin-indent-details
	 * Controller to view open indents by Admin
	 *  
	 * */
	@RequestMapping(value = "/admin-indent-details")
	public ModelAndView adminIndentDetails(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		List<IndentDetails> indentDetailsDB = indentOrderService.getOpenIndentDetailsForAdmin(/*"5"*/"6");
		model.addAttribute("indentOrders", indentDetailsDB);
		return new ModelAndView("adminViewOpenIndents");
	}
	
	
	//export-indent-details
	/*@RequestMapping(value = "/export-indent-details")
	public void exportIndentDetails(Model model,HttpServletRequest request,ServletContext context ,HttpServletResponse response,HttpSession session,User user,BindingResult bindingResult)
	{
		
		indentOrderService.downloadIndentinExcel(request,response, context);
	}*/
	
	@RequestMapping(value = "/customer-indent-details")
	public ModelAndView customerIndentDetails(Model model,User user,HttpSession session,BindingResult bindingResult, String customerId)
	{
		List<IndentDetails> indentDetailsDB = indentOrderService.getcustomerIndentDetails(customerId);
		
		
		model.addAttribute("indentOrders", indentDetailsDB);
	
		return new ModelAndView("customerViewIndents");
	}
	@RequestMapping(value = "/openIndents")
	public ModelAndView openIndentsDetails(Model model,User user,HttpSession session,BindingResult bindingResult, String customerId)
	{
		List<IndentDetails> indentDetailsDB = indentOrderService.getcustomerIndentDetails(user.getId());
		
		
		model.addAttribute("indentOrders", indentDetailsDB);
		return new ModelAndView("customerViewIndents");
	}
	
	@RequestMapping(value = "/admin-confirm-Indent")
	public String AdminconfirmIndentOrder(HttpServletResponse response,Model model,HttpSession session,String indentOrderId){		
		
	//	User user = (User)session.getAttribute("user");
      indentOrderService.confirmIndentOrder(user.getId(),indentOrderId);
      
      
	//userservice.getUserByEmailId(indentOrderId);
		// if order confirmed , then mail send logic goes here
		
		
	//List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId(),"5");
		
		System.out.println("confirm-Indent in db");
		//model.addAttribute("indentOrders", indentDetailsDB);
		
		
		return "redirect:/admin-account-details";
		
	}
	
	@RequestMapping(value = "/admin-cancel-Indent")
	public String AdmincancelIndentOrder(HttpServletResponse response,Model model,HttpSession session,String indentOrderId){		
		
		
		User user = (User)session.getAttribute("user");
		
		indentOrderService.admincancelIndentOrder(indentOrderId);
				
		//List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId(),"5");
		
		System.out.println("cancel-Indent in db");
		//model.addAttribute("indentOrders", indentDetailsDB);
		
		List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(user.getId());
		
		System.out.println("From Account Details Controller");
		model.addAttribute("indentOrders", indentDetailsDB);
		System.out.println("In account details controller");
		//return new ModelAndView("accountdetails");
		
		return "redirect:/admin-account-details";
		
	}
	@RequestMapping(value = "/admin-processed-indents")
	public ModelAndView adminprocessedIndentDetails(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		List<IndentDetails> indentDetailsDB = indentOrderService.getadminprocesedIndentDetailsForAdmin(/*"5"*/"7");
		model.addAttribute("indentOrders", indentDetailsDB);
		return new ModelAndView("recentlyplaced1");
	}
	
	@RequestMapping(value = "/admin-cancelled-indents")
	public ModelAndView adminuserCancelledIndentOrders(HttpServletResponse response,Model model,HttpSession session){		
				
		List<IndentDetails> cancelledOrderList =  indentOrderService.admincancelledIndentOrders("4");		
		model.addAttribute("cancelledOrderList", cancelledOrderList);	
		return new ModelAndView("adminIndentCancellation"); 
	}
	
	@RequestMapping(value = "/recently-placed-indents")
	public ModelAndView recentplacedIndentDetails(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		List<IndentDetails> indentDetailsDB = indentOrderService.getrecentplacedIndentDetailsForAdmin(/*"5"*/"7");
		model.addAttribute("indentOrders", indentDetailsDB);
		return new ModelAndView("recentlyplacedindents");
	}
	
	@RequestMapping(value = "/Deleted-indents")
	public ModelAndView deletedIndentOrders(HttpServletResponse response,Model model,HttpSession session){		
				
		List<IndentDetails> cancelledOrderList =  indentOrderService.deletedIndentOrders("4");		
		model.addAttribute("cancelledOrderList", cancelledOrderList);	
		return new ModelAndView("deletedindents"); 
	}

	
	
}
