package com.minds.servo.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

//import com.minds.servo.iservice.ExcelService;
import com.minds.servo.iservice.IndentOrderService;
import com.minds.servo.iservice.ProductService;
import com.minds.servo.iservice.UserService;
import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.Product;
import com.minds.servo.model.User;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	// @Autowired ExcelService excelservice;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired private UserService userService;	
	@Autowired private ProductService productService;
	@Autowired private IndentOrderService indentOrderService;

		
	@RequestMapping(value = "/home")
	public ModelAndView getIndex(){		
		
		return new ModelAndView("login");    //  create View product page and iterate  productList
		
	}
	
	
	@RequestMapping(value = "/logOut")
	public ModelAndView logOut(Model model,HttpSession session){		
		
		session.invalidate();
		return new ModelAndView("login");    //  create View product page and iterate  productList
		
	} 
	
	@RequestMapping(value = "/servo-details")
	public String servoDetails(Model model,HttpSession session,User user){
				
		String email = user.getEmailid();
		String password = user.getPassword();		
		User userDBUser = userService.getUserByEmailId(email);
		
		if(userDBUser!=null)			
		{	
			if(userDBUser.getPassword().equals(password)){
				return "redirect:/get-all-products";
			}
			else
				model.addAttribute("message", "Invalid Password");
				return "redirect:/home";
		}
		else 
			return "redirect:/home";		
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		return "redirect:/home";
	}
	
	

	@RequestMapping(value = "/validate-user")
	public String validateLogin(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		String email = user.getEmailid();
		String password = user.getPassword();		
		User userDBUser = userService.getUserByEmailId(email);
		session.setAttribute("user", userDBUser);
		if(userDBUser.getPassword().equals(password)){
			if(userDBUser.getUsertype().equalsIgnoreCase("1"))
			{
				return "redirect:/account-details";
			}else if(userDBUser.getUsertype().equalsIgnoreCase("3")) {
				return "redirect:/admin-account-details";
			}	
		}
		else
		{
			model.addAttribute("message", "Invalid Password");
			return "redirect:/";
		}
			
		return "";
	}
	
	@RequestMapping(value = "/admin-account-details")
	public ModelAndView adminAccountDetails(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		return new ModelAndView("adminAccountDetails");
	}
	
	
	
	@RequestMapping(value = "/account-details")
	public ModelAndView accountDetails(@ModelAttribute("product") Product product,Model model,HttpSession session,User user,BindingResult bindingResult)
	{
		String email = user.getEmailid();
		String password = user.getPassword();		
		User userFromSession = (User)session.getAttribute("user");
				
		if(userFromSession!=null)			
		{	
				// get products item name from DB
				List<Product> products = productService.getAllProducts();
				
				model.addAttribute("productList", products);
				model.addAttribute("user", userFromSession);
				session.setAttribute("user", userFromSession);
			//	return new ModelAndView("accountdetails");
			List<IndentDetails> indentDetailsDB = indentOrderService.getIndentOrderDetails(userFromSession.getId());
				
				System.out.println("From Account Details Controller");
				model.addAttribute("indentOrders", indentDetailsDB);
				System.out.println("In account details controller");
				if(userFromSession.getUsertype().equalsIgnoreCase("1"))
				{
					return new ModelAndView("accountdetails");
				}else if(userFromSession.getUsertype().equalsIgnoreCase("3")) {
					return new ModelAndView("adminAccountDetails");
				}
			}
			return new ModelAndView("login");
		}
	
	@RequestMapping(value = "view-customers", method = RequestMethod.GET)
	public ModelAndView viewCustomers(Locale locale, Model model,HttpSession session)
	{
		logger.info("Welcome home! The client locale is {}.", locale);
		
		List<User> user = userService.getCustomers("1");
		model.addAttribute("userList",user);
	return new ModelAndView("adminViewCustomers");	
	}
	
	
	@RequestMapping(value = "template-page", method = RequestMethod.GET)
	public ModelAndView templatePage(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		return new ModelAndView("templatePage");	
	}
	
	
	
}
