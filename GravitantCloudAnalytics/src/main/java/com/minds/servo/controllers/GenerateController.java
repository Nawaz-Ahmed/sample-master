package com.minds.servo.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GenerateController
{

	
	 @RequestMapping(value = "/generateExcel", method = RequestMethod.GET)
	    public String showform(Locale locale, Model model) {
	        return "generateExcel";
	    }

	    @RequestMapping(value = "/excelfile", method = RequestMethod.GET)
	    public void downloadnotepadfile(HttpServletRequest request,
	            HttpServletResponse response) 
	    {
	    	

			XSSFWorkbook workbook = new XSSFWorkbook();
		    XSSFSheet sheet = workbook.createSheet("Calculate Simple Interest");
		 
		    Row header = sheet.createRow(0);
		    header.createCell(0).setCellValue("SKU");
		    header.createCell(1).setCellValue("Qty");
		    header.createCell(2).setCellValue("Total Qty in Ltr/Kg");
		    header.createCell(3).setCellValue("Cum-Duty/RSP Price");
		    header.createCell(4).setCellValue("Gross Amount");
		    header.createCell(5).setCellValue("Discount Amount");
		     
		    Row dataRow = sheet.createRow(1);
		    dataRow.createCell(0).setCellValue("Servo1");
		    dataRow.createCell(1).setCellValue(1d);
		    dataRow.createCell(2).setCellValue(10d);
		    dataRow.createCell(3).setCellValue(20000d);
		    dataRow.createCell(4).setCellFormula("b2*d2");
		    dataRow.createCell(5).setCellFormula("(e2-(e2/10))");
		     
		    
		    Row dataRow0 = sheet.createRow(2);
		    
		    Row dataRow1 = sheet.createRow(3);
		    dataRow1.createCell(0).setCellValue("test");
		    dataRow1.createCell(1).setCellValue(1d);
		    dataRow1.createCell(2).setCellValue(10d);
		    dataRow1.createCell(3).setCellValue(20000d);
		    dataRow1.createCell(4).setCellFormula("b2*d2");
		    dataRow1.createCell(5).setCellFormula("(e2-(e2/10))");
		     
		    
		    
		    
		    try {
		        FileOutputStream out =  new FileOutputStream(new File("D:\\formulaDemo.xlsx"));
		        workbook.write(out);
		        out.close();
		        System.out.println("Excel written successfully..");
		        
		        readSheetWithFormula();
		         
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
		
		public static void readSheetWithFormula()
		{
			try
			{
				FileInputStream file = new FileInputStream(new File("formulaDemo.xlsx"));

				//Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(file);

				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
				
				//Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				//Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) 
				{
					Row row = rowIterator.next();
					//For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
					while (cellIterator.hasNext()) 
					{
						Cell cell = cellIterator.next();
						//If it is formula cell, it will be evaluated otherwise no change will happen
						switch (evaluator.evaluateInCell(cell).getCellType()) 
						{

						case Cell.CELL_TYPE_NUMERIC:
								System.out.print(cell.getNumericCellValue() + "\t\t");
								break;
							case Cell.CELL_TYPE_STRING:
								System.out.print(cell.getStringCellValue() + "\t\t");
								break;
							case Cell.CELL_TYPE_FORMULA:
								//Not again
								break;
						}
					}
					System.out.println("");
				}
				
				file.close();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		
	    	
}
}
