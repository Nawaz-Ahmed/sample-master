

package com.minds.servo.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minds.servo.dao.IndentOrderDAO;
import com.minds.servo.helper.ServoConstants;
import com.minds.servo.iservice.IndentOrderService;
import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.User;

@Service
public class IndentOrderServiceImpl implements IndentOrderService{
	
	
	@Autowired private IndentOrderDAO indentOrderDAO;
		
	public void initiateIndentOrder(IndentDetails indentDetails)
	{
		indentOrderDAO.insertInitiateIndentOrders(indentDetails);
		System.out.println("done with insertion");
		
	}
	
	public List<IndentDetails> getIndentOrderDetails(String userid)
	{
		List<IndentDetails> indentDetails = indentOrderDAO.getIndentOrdersByUserAndStatus(userid,ServoConstants.NEW_INDENT_ORDERS_STATUS); 
		return indentDetails;
	}

	
	public void cancelIndentOrder(String userid, String indentOrderId) {
		// TODO Auto-generated method stub
		
		indentOrderDAO.cancelIndentOrderByUserAndOrderId(userid,indentOrderId);
		
	}
	
	public void confirmIndentOrder(String userid, String indentOrderId) 
	
	{
		
		indentOrderDAO.confirmIndentOrderByUserAndOrderId(userid,indentOrderId);
		
	}
	
	public List<IndentDetails> confirmedIndentOrders(User user){
		
		List<IndentDetails> indentDetails = indentOrderDAO.getIndentOrdersByUserAndStatus(user.getId(),ServoConstants.CONFIRMED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}
	
	public List<IndentDetails> getOpenIndentDetailsForAdmin(String orderstatus) {
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getOpenIndentDetailsForAdmin(ServoConstants.CONFIRMED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	public List<IndentDetails> cancelledIndentOrders(User user) {
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getIndentOrdersByUserAndStatus1(user.getId(),ServoConstants.CANCELLED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	/*public List<IndentDetails> getcustomerIndentDetails(String userid) {
		// TODO Auto-generated method stub
		
		return null;
	}
*/
	

	public List<IndentDetails> getcustomerIndentDetails(String customerId) {
		List<IndentDetails> indentDetails = indentOrderDAO.getcustomerIndentDetails(customerId); 
		return indentDetails;
	}

	public void AdminconfirmIndentOrder(String indentOrderId) {
		// TODO Auto-generated method stub
		indentOrderDAO.adminconfirmIndentOrderByUserAndOrderId(indentOrderId);
	}

	public void admincancelIndentOrder(String indentOrderId) {
		// TODO Auto-generated method stub
		indentOrderDAO.admincancelIndentOrderByUserAndOrderId(indentOrderId);
	}

	public List<IndentDetails> getadminprocesedIndentDetailsForAdmin(String orderstatus)
	{
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getadminprocessedIndentDetailsForAdmin(ServoConstants.ADMIN_CONFIRMED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	public List<IndentDetails> admincancelledIndentOrders(String orderstatus) {
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getadmincancelledIndentOrdersByUserAndStatus1(ServoConstants.ADMIN_CANCELLED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	public List<IndentDetails> getrecentplacedIndentDetailsForAdmin(
			String orderstatus) {
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getrecentlyplacedIndentDetailsForAdmin(ServoConstants.ADMIN_CONFIRMED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	public List<IndentDetails> deletedIndentOrders(String orderstatus) {
		// TODO Auto-generated method stub
		List<IndentDetails> indentDetails = indentOrderDAO.getdeletedindentents(ServoConstants.ADMIN_CANCELLED_INDENT_ORDERS_STATUS);
		return indentDetails;
	}

	
	

	
	
	

	

	

	

	
}