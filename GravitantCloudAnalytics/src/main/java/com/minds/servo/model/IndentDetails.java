package com.minds.servo.model;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

@Component
public class IndentDetails {
	
	private String id;
	private String skuCode;
	private String itemName;
	private String pricePerPack;
	private String mrp;
	private String qty;
	private String total;
	private String remarks;
	private String userid;
	private String Servouser_id;
	private String orderstatus;
	private String rowstate;
	private String custOrgid;
	private String userTypeid;
	private Timestamp createdDate;
	private Timestamp lastUpdatedDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSkuCode() {
		return skuCode;
	}
	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getPricePerPack() {
		return pricePerPack;
	}
	public void setPricePerPack(String pricePerPack) {
		this.pricePerPack = pricePerPack;
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public String getRowstate() {
		return rowstate;
	}
	public void setRowstate(String rowstate) {
		this.rowstate = rowstate;
	}
	public String getMrp() {
		return mrp;
	}
	public void setMrp(String mrp) {
		this.mrp = mrp;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getServouser_id() {
		return Servouser_id;
	}
	public void setServouser_id(String servouser_id) {
		Servouser_id = servouser_id;
	}
	public String getCustOrgid() {
		return custOrgid;
	}
	public void setCustOrgid(String custOrgid) {
		this.custOrgid = custOrgid;
	}
	public String getUserTypeid() {
		return userTypeid;
	}
	public void setUserTypeid(String userTypeid) {
		this.userTypeid = userTypeid;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Timestamp lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	
	
	

}

