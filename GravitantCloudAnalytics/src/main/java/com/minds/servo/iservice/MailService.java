package com.minds.servo.iservice;

import java.util.List;

import org.springframework.stereotype.Component;

import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.User;


@Component
public interface MailService
{
	
	public void sendPlainMail(IndentDetails indentDetails,String mail);

	public void sendPlainMail(List<User> user, User userByEmailId);

	

}
