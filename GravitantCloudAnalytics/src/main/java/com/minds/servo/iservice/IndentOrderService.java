package com.minds.servo.iservice;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.minds.servo.model.IndentDetails;
import com.minds.servo.model.User;

@Component
public interface IndentOrderService {

	public void initiateIndentOrder(IndentDetails indentDetails);
	//cancelIndentOrder
	public void cancelIndentOrder(String userid, String indentOrderId);
	public List<IndentDetails> getIndentOrderDetails(String userid);
	//confirmIndentOrder
	public void confirmIndentOrder(String userid, String indentOrderId);
	
	public List<IndentDetails> confirmedIndentOrders(User user);
	public List<IndentDetails> getOpenIndentDetailsForAdmin(String orderstatus);
	public List<IndentDetails> cancelledIndentOrders(User user);
	public List<IndentDetails> getcustomerIndentDetails(String customerId);
	public void AdminconfirmIndentOrder(String indentOrderId);
   public void admincancelIndentOrder( String indentOrderId);
   public List<IndentDetails> getadminprocesedIndentDetailsForAdmin(String orderstatus);
        public List<IndentDetails> admincancelledIndentOrders(String orderstatus);
	public List<IndentDetails> getrecentplacedIndentDetailsForAdmin(String orderstatus);
	public List<IndentDetails> deletedIndentOrders(String orderstatus);
	
	
	
	
	
}



